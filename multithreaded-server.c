#include <stdio.h>
#include <sys/socket.h> // socket etc.
#include <arpa/inet.h>  // htons, inet_addr, IPPROTO_TCP, sockaddr_in
#include <stdlib.h> // exit
#include <string.h> // strlen, memset
#include <unistd.h>  // close
#include <errno.h>
#include <pthread.h>

#define BUF_SIZE 800
#define SERVER_PORTNUM 9000
#define QUEUE_SIZE  1

typedef struct thread_arg {
    int sockfd;
} thread_arg;

void erro_handler(const char *message)
{
    fprintf(stderr, "%s", message);
    exit(1);
}

void* client_thread(void* arg) {
    thread_arg *sock_data = (thread_arg*)arg;
    int client_sock = sock_data -> sockfd;
    free(arg);

    // receive message from the client
    char buf[BUF_SIZE];
    int received_bytes = 0;
    printf("Received messages: ");
    while(1) {
        // receive messages from the server
        if ((received_bytes = recv(client_sock, buf, sizeof(buf)-1, 0)) < 0) {
            perror("receive error");
            exit(EXIT_FAILURE);
        }
        else if (received_bytes == 0) break; // connection closed by client
        buf[received_bytes] = '\0';
        printf("%s", buf);

        // send messages to the client
        int total_sent_size;
        /*
          send message (echo reply)
        */
       int message_size = strlen(buf);
        for(total_sent_size = 0; total_sent_size < message_size;){
            int sent_size;
            if((sent_size = send(client_sock, &buf[total_sent_size], message_size - total_sent_size, 0)) < 0){
                close(client_sock); erro_handler("send() failed.\n");
            }
            total_sent_size += sent_size;
        }
    }

    // close the client socket.
    close(client_sock);
    return NULL;
}

int main( int argc, char *argv[]) {
    int serv_sock;
    // create a socket.
    if ((serv_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) goto ERROR;

    // initialize
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(SERVER_PORTNUM);

    // bind
    if (bind(serv_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) goto ERROR;

    // listen
    printf("listening on port %d\n", SERVER_PORTNUM);
    if (listen(serv_sock, QUEUE_SIZE) < 0) goto ERROR;

    struct sockaddr_in client_addr;
    thread_arg *client;
    socklen_t addr_len = sizeof(client_addr);
    while(1) {
        client = (thread_arg*)malloc(sizeof(thread_arg));
        if ((client->sockfd = accept(serv_sock, (struct sockaddr *)&client_addr, &addr_len )) < 0 ) goto ERROR;

        pthread_t tid;
        if(pthread_create(&tid, NULL, client_thread, client)) {
            perror("pthread create");
            goto ERROR2;
        }
        // detach thread
        pthread_detach(tid);
    }
    
    // close the server socket.
    close(serv_sock);
    return 0;

ERROR2:
    if (client!=NULL && client->sockfd >= 0) close(client->sockfd);
    if (client!=NULL) free(client);

ERROR:
    fprintf(stderr, "ERROR: %s\n", strerror(errno));
    if (serv_sock >= 0) close(serv_sock);
    return(EXIT_FAILURE);
}

